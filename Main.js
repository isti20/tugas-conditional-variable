//Conditional Variabel

//1. Assignment variabel dengan pengecekan kondisi tertentu
const hargaBuku = 90000;
const kategori = hargaBuku >= 70000 ? "Mahal" : "Murah";
console.log(kategori); //output: Mahal

const nilaiUjian = 70;
const nilaiLulus = nilaiUjian > 69 && "Lulus";
const nilaiTidakLulus = nilaiUjian < 70 || "Tidak lulus";
console.log(nilaiLulus); //output: Lulus
console.log(nilaiTidakLulus) //output: Tidak lulus

const nilaiku = 80;
const hasilku = (nilaiku >= 40) ? ((nilaiku >= 70) ? 'nilai A' : 'nilai B') : 'nilai C';
console.log(hasilku) //output: nilai A

function disc(member) {
    return (member ? "Rp. 5000" : "Rp. 0");
}
console.log(disc(true)); //output: Rp. 5000
console.log(disc(false)); //outpur: Rp. 0
console.log(disc(null)); //output: Rp. 0

//2. Manipulasi data array dengan method map() dan filter()
const codes = [101, 212, 667];
let mathCodes = codes.map((code) => {
    return `math${code}`;
})

console.log(mathCodes);
//output: ["math101", "math212", "math667"]

const numbers = [101, 212, 667];
let memberCodes = numbers.map(function(code) {
    return `Member${code}`;
})

console.log(memberCodes);
//output: ["Member101", "Member212", "Member667"]

const namaMember = ["Safira", "Andena", "Sofia"];
let panjangNama = namaMember.map((nama) => nama.length);
console.log(panjangNama);
//output: [6, 6, 5]

const angka = [9, 25, 36, 49];
let akarKuadrat = angka.map((hasil) => {
    return Math.sqrt(hasil);
});

console.log(akarKuadrat);
//output: [3, 5, 6, 7]

const nilaiAkhir = [70, 80, 90, 100];

nilaiAkhir.map((nilai, indexNilai, array) => {
    console.log("Nilai " + nilai);
    console.log("Index " + indexNilai);
    console.log("Array " + array);
    return nilaiAkhir;
})
//output: Nilai 70 Index 0 Array 70,80,90,100, dst

const siswa = [
    {namaDepan : "Safira", namaBelakang: "Salsabilla"},
    {namaDepan : "Andena", namaBelakang: "Digantara"},
    {namaDepan : "Sofia", namaBelakang: "Oktadian"}
];

let namaLengkap = siswa.map(siswa => {
    return `${siswa.namaDepan} ${siswa.namaBelakang}`;
})

console.log(namaLengkap);
//output: ["Safira Salsabilla", "Andena Digantara", "Sofia Oktadian"]

const listNilai = [
  {
    nama: "Safira",
    nilai: 80,
    tahun : 2020,
  },
  {
    nama: "Andena",
    nilai: 70,
    tahun: 2022,
  },
  {
    nama: "Sofia",
    nilai: 90,
    tahun: 2021,
  },
];

const nilaiMath = listNilai.map((value) => value.nama);
console.log(nilaiMath);
//output: ["Safira", "Andena", "Sofia"]

const hasilSiswa = listNilai
.filter((siswa) => siswa.tahun > 2020)
.map((siswa) => {
    if (siswa.nilai > 70){
        return siswa.nama + ': Lulus'
    } else return siswa.nama + ": Tidak lulus"
});

console.log(hasilSiswa);
//output: ["Andena: Tidak lulus", "Sofia: Lulus"]

const nilaiTambahan = listNilai.map((newValue) => newValue.nilai+5);
console.log(nilaiTambahan);
//output: [85, 75, 95]

const nilaiSiswa = listNilai.filter((Value) => Value.nilai);
console.log(nilaiSiswa);
//output: [Object, Object, Object]
// 0: Object => nama: "Safira" nilai: 80 tahun: 2020
// 1: Object => nama: "Andena" nilai: 70 tahun: 2022
// 3: Object => nama: "Sofia" nilai: 90 tahun: 2021

const namaSiswa = listNilai.filter((Value) => Value.nama);
console.log(namaSiswa);
//output: [Object, Object, Object]
// 0: Object => nama: "Safira" nilai: 80 tahun: 2020
// 1: Object => nama: "Andena" nilai: 70 tahun: 2022
// 3: Object => nama: "Sofia" nilai: 90 tahun: 2021

